package org.sber.state;

public enum State {
    NEW {
        @Override
        public State nextState() {
            return DRAFT;
        }
    },
    DRAFT{
        @Override
        public State nextState() {
            return READY;
        }
    },
    READY{
        @Override
        public State nextState() {
            return READY;
        }
    };

    public abstract State nextState();

}
