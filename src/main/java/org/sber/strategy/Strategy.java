package org.sber.strategy;

import org.sber.Payment;

public interface Strategy {

    double getCommission(double summa);
}
