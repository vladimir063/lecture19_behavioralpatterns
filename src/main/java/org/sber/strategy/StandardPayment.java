package org.sber.strategy;

import org.sber.Payment;

public class StandardPayment implements Strategy {

    @Override
    public double getCommission(double summa) {
        return  summa > 1000 ? 10 : summa * 0.01 ;
    }
}
