package org.sber;

import java.util.ArrayList;
import java.util.List;

public class Cart implements Observable{

    private List<Observer> observers;
    private final List<Payment> payments;

    public Cart() {
        this.payments = new ArrayList<>();
        this.observers = new ArrayList<>();
    }
    public List<Payment> getPayments() {
        return payments;
    }
    public void addPayment(Payment payment) {
        payments.add(payment);
        payment.setState(payment.getState().nextState());
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(payments.get(payments.size() - 1)));
    }
}
