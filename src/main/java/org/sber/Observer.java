package org.sber;

public interface Observer {

    void update(Payment payment);
}
