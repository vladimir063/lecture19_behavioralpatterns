package org.sber;


import org.sber.state.State;
import org.sber.strategy.Strategy;

public class Payment  {

    private final String name;
    private final double summa;
    private double commission;
    private State state;
    private Strategy strategy;

    public Payment(String name, double summa, Strategy strategy) {
        this.name = name;
        this.summa = summa;
        this.state = State.NEW;
        this.strategy = strategy;
    }

    public double getSumma() {
        return summa;
    }

    public void calcCommission() {
        state = state.nextState();
        commission = strategy.getCommission(summa);
    }

    public double getFullAmount() {
        return summa + commission;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "name='" + name + '\'' +
                ", summa=" + summa +
                ", commission=" + commission +
                ", state='" + state + '\'' +
                '}';
    }
}



