package org.sber;

import org.sber.state.State;
import org.sber.strategy.CharityPayment;
import org.sber.strategy.StandardPayment;

import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Cart cart = new Cart();
        ConsoleListener consoleListener = new ConsoleListener();
        cart.registerObserver(consoleListener);
        StandardPayment standardPayment = new StandardPayment();
        CharityPayment charityPayment = new CharityPayment();

        Payment payment1 = new Payment("Payment1", 100, standardPayment );
        Payment payment2 = new Payment("Payment2", 500, standardPayment);
        Payment payment3 = new Payment("Payment3", 2000 ,standardPayment );
        Payment payment4 = new Payment("Payment4", 5000, standardPayment);
        Payment payment5 = new Payment("Платеж на благотворительность", 50000, charityPayment);

        payment1.calcCommission();
        payment2.calcCommission();
        payment3.calcCommission();
        payment4.calcCommission();
        payment5.calcCommission();

        //Добавление в корзину
        cart.addPayment(payment1); // Перевод переходит в состояние "READY"
        cart.addPayment(payment2);
        cart.addPayment(payment3);
        cart.addPayment(payment4);
        cart.addPayment(payment5);


        System.out.println("Итого в корзине: " +
                cart.getPayments().stream().collect(Collectors.summarizingDouble(Payment::getFullAmount)));
    }

}


