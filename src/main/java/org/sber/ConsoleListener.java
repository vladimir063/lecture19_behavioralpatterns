package org.sber;

public class ConsoleListener implements Observer {

    @Override
    public void update(Payment payment) {
        System.out.println("Добавлен платёж: " + payment);
    }
}
